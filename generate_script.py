import os
import random

proj_folder = "/home/ricky/euristici/algoritmi_euristici/"
path_dati = proj_folder + "dati/"
programma = proj_folder + "code/build/metaeuristiche"

try:
  os.mkdir(path=proj_folder + "scripts")
except FileExistsError:
  pass

try:
  os.mkdir(path=proj_folder + "results")
except FileExistsError:
  pass

results_path = proj_folder + "results/"

def grasp_value():
  alpha = [0.05, 0.1, 0.15]
  iterTot = [10, 20, 30]
  
  for i in alpha:
    for j in iterTot:
      filename = "scripts/grasp_val_{}_{}.sh".format(i, j)
      fd = open(filename, "w")
      seed = random.randint(-2147483648, -1)
      fd.write("#!/bin/bash\n\n")
      outfile = "{}grasp_val_{}_{}.out".format(results_path, i, j)
      fd.write("touch {}\n".format(outfile))
      fd.write("echo \"Istanza Tempo Valore\" > {}\n".format(outfile))
      for el in sorted(os.listdir(path_dati)):
        dati = path_dati + el
        fd.write("echo \"grasp_val_{}_{}: analizzo istanza {}...\"\n".format(i, j, el))
        fd.write("{} {} {} {} {} {} >> {}\n".format(programma, dati, "-gv", i, j, seed, outfile))
      fd.close()
      os.chmod(filename, 0o755)

def grasp_card():
  card = [2, 4, 8]
  iterTot = [10, 20, 30]
  
  for i in card:
    for j in iterTot:
      filename = "scripts/grasp_card_{}_{}.sh".format(i, j)
      fd = open(filename, "w")
      seed = random.randint(-2147483648, -1)
      fd.write("#!/bin/bash\n\n")
      outfile = "{}grasp_card_{}_{}.out".format(results_path, i, j)
      fd.write("touch {}\n".format(outfile))
      fd.write("echo \"Istanza Tempo Valore\" > {}\n".format(outfile))
      for el in sorted(os.listdir(path_dati)):
        dati = path_dati + el
        fd.write("echo \"grasp_card_{}_{}: analizzo istanza {}...\"\n".format(i, j, el))
        fd.write("{} {} {} {} {} {} >> {}\n".format(programma, dati, "-gc", i, j, seed, outfile))
      fd.close()
      os.chmod(filename, 0o755)

def ant():
  generations = [4, 8, 12]
  ants = [4, 8, 12]
  q = [0.1, 0.2]
  oblivion = [0.1, 0.2, 0.3]
  for i in generations:
    for j in ants:
      for k in q:
        for l in oblivion:
          filename = "scripts/ant_{}_{}_{}_{}.sh".format(i, j, k, l)
          fd = open(filename, "w")
          seed = random.randint(-2147483648, -1)
          fd.write("#!/bin/bash\n\n")
          outfile = "{}ant_{}_{}_{}_{}.out".format(results_path, i, j, k, l)
          fd.write("touch {}\n".format(outfile))
          fd.write("echo \"Istanza Tempo Valore\" > {}\n".format(outfile))
          for el in sorted(os.listdir(path_dati)):
            dati = path_dati + el
            fd.write("echo \"ant_{}_{}_{}_{}: analizzo istanza {}...\"\n".format(i, j, k, l, el))
            fd.write("{} {} {} {} {} {} {} {} >> {}\n".format(programma, dati, "-a", i, j, k, l, seed, outfile))
          fd.close()
          os.chmod(filename, 0o755)

def executor():
  folder = proj_folder + "scripts"
  filename = "execute_scripts.sh"
  fd = open(filename, "w")
  fd.write("#!/bin/bash\n\n")
  for el in sorted(os.listdir(folder)):
    fd.write("{}/{}\n".format(folder, el))
  fd.close()
  os.chmod(filename, 0o755)
  
grasp_value()
grasp_card()
ant()
executor()