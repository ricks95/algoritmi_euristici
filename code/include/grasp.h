#ifndef __GRASP_H
#define __GRASP_H

#include "greedy.h"
#include "partition.h"
#include "random.h"
#include "solution.h"

int best_random_additional_point_value (solution_t *px, data_t *pI, double alpha, long *pidum);
int best_random_additional_point_card (solution_t *px, data_t *pI, int card, long *pidum);

void grasp_RCL_value (data_t *pI, solution_t *px, double alpha, int iterTot, long *pidum);
void grasp_RCL_cardinality (data_t *pI, solution_t *px, int card, int iterTot, long *pidum);

#endif