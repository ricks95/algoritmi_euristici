#ifndef __ANT_H
#define __ANT_H

#include "alloc.h"
#include "greedy.h"
#include "partition.h"
#include "random.h"
#include "solution.h"


void scambia_double (double *pa, double *pb);
void sort_double (int n, double *V_val, int *V_ind);
int extract_next_point (int *indices, int dimension, double q, long *seed);
int best_additional_point_with_trace (solution_t *px, data_t *pI, double q, double *trace, long *seed);
void randomized_greedy (data_t *pI, solution_t *px, double q, double *trace, long *seed);
double* init_trace (size_t n, double value);
void intensify_trace (double *trace, solution_t *px, int dimension, double obl);
void diversify_trace (double *trace, solution_t *px, int dimension, double obl);
void ant_colony(data_t *pI, solution_t *px, int generations, int ants, double q, double obl, long *seed);

#endif