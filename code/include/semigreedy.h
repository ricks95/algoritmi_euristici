#ifndef __SEMIGREEDY_H
#define __SEMIGREEDY_H

#include "greedy.h"

void semigreedy (data_t *pI, solution_t *px, double alpha, long *pidum);

#endif /* __SEMIGREEDY_H */
