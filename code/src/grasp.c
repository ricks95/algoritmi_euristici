#include "grasp.h"

/* Seleziona un punto con probabilita' uniforme dopo aver costruito una RCL per valore,
   con tutti i punti di valore compreso fra vMax e vMax - alpha*(vMax - vMin). Il parametro
   alpha decide quanto ampia debba essere la lista, maggiore alpha maggiore sara' la
   cardinalita' della lista da cui estrarre il valore a caso.
*/
int best_random_additional_point_value (solution_t *px, data_t *pI, double alpha, long *pidum) {
  point p;
  int *d;
  int i, i_max;
  int *ind;
  int num, tot;

  d = int_alloc(pI->n+1);
  ind = int_alloc(pI->n+1);

  tot = 0;
  for (p = first_point_out(px); !end_point_list(p,px); p = next_point(p,px)) {
    tot++;
    i = get_index(p,px);
    d[tot] = dist_from_solution(i,px,pI);
    ind[tot] = i;
  }

  num = partition_by_value(tot,d,ind,alpha);

  i_max = ind[rand_int(1,num,pidum)];

  free(d);
  free(ind);
  return i_max;
}

/* Seleziona un punto con probabilita' uniforme dopo aver costruito una RCL per cardinalita',
   con quest'ultima scelta dall'utente. Cardinalita' pari ad 1 corrisponde all'euristica
   costruttiva base, pari alla dimensione della lista di punti corrisponde ad una random walk
*/
int best_random_additional_point_card (solution_t *px, data_t *pI, int card, long *pidum) {
  point p;
  int *d;
  int i, i_max, dim;
  int *ind;
  int num, tot;

  dim = pI->n+1;
  d = int_alloc(dim);
  ind = int_alloc(dim);

  tot = 0;
  for (p = first_point_out(px); !end_point_list(p,px); p = next_point(p,px)) {
    tot++;
    i = get_index(p,px);
    d[tot] = dist_from_solution(i,px,pI);
    ind[tot] = i;
  }
  
  partition_by_card(dim, d, ind, card);

  i_max = ind[rand_int(1,card,pidum)];

  free(d);
  free(ind);
  return i_max;
}

/* Esegue la procedura GRASP con RCL per valore, con parametro alpha e
   iterTot iterazioni totali
*/
void grasp_RCL_value (data_t *pI, solution_t *px, double alpha, int iterTot, long *pidum) {
  int i, iter;
  solution_t xCurr;

  create_solution(pI->n,&xCurr);
  copy_solution(px,&xCurr);

  for (iter = 0; iter < iterTot; iter++) {
    while (xCurr.card_x < pI->k) {
      i = best_random_additional_point_value(&xCurr,pI,alpha,pidum);
      move_point_in(i,&xCurr,pI);
    }
    if (xCurr.f > px->f) {
      copy_solution(&xCurr,px);
    }
    clean_solution(&xCurr);
  }
  destroy_solution(&xCurr);
}

/* Esegue la procedura GRASP con RCL per cardinalita', con card dimensione della RCL e
   iterTot iterazioni totali
*/
void grasp_RCL_cardinality (data_t *pI, solution_t *px, int card, int iterTot, long *pidum) {
  int i, iter;
  solution_t xCurr;

  create_solution(pI->n,&xCurr);
  copy_solution(px,&xCurr);

  for (iter = 0; iter < iterTot; iter++) {
    while (xCurr.card_x < pI->k) {
      i = best_random_additional_point_card(&xCurr,pI,card,pidum);
      move_point_in(i,&xCurr,pI);
    }
    if (xCurr.f > px->f) {
      copy_solution(&xCurr,px);
    }
    clean_solution(&xCurr);
  }
  destroy_solution(&xCurr);
}