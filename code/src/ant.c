#include "ant.h"

/* Funzione che scambia due double */
void scambia_double (double *pa, double *pb) {
  double temp;

  temp = *pa;
  *pa = *pb;
  *pb = temp;
}

/* Ordina parallelamente due vettori, uno di double contenente i valori
   e uno di int contenente gli indici */
void sort_double (int n, double *V_val, int *V_ind) {
  int i, j;

  for (i = 1; i <= (n - 1); i++) {
    for (j = i+1; j <= n; j++) {
      if (V_val[j] > V_val[i]) {
        scambia_double(&V_val[j],&V_val[i]);
        scambia(&V_ind[j],&V_ind[i]);
      }
    }
  }
}

/* Estrae un indice: restituisce quello corrente con probabilita' 1-q,
   altrimenti prova il successivo. Nel caso peggiore restituisce
   l'ultimo della lista */
int extract_next_point (int *indices, int dimension, double q, long *seed) {
  int rand, i;
  double check;

  for (i = 1; i < (dimension - 1); i++) {
    rand = rand_int(1, 100, seed);
    check = rand / 100.0;
    if (check <= (1-q)) {
      return indices[i];
    }
  }
  return indices[i-1];
}

/* Sceglie il miglior punto fra quelli esterni alla soluzione corrente.
   La scelta avviene basandosi sulla distanza, ma anche sulla traccia.
   Il punto viene incluso con probabilita' 1-q, altrimenti si passa al
   successivo
*/
int best_additional_point_with_trace (solution_t *px, data_t *pI, double q, double *trace, long *seed) {
  point p;
  double *distances;
  int i, i_extracted, num, tot, dim;
  int *ind;

  dim = (px->card_N) - (px->card_x);
  distances = double_alloc(dim+1);
  ind = int_alloc(dim+1);
  tot = 0;

  for (p = first_point_out(px); !end_point_list(p,px); p = next_point(p,px)) {
    tot++;
    i = get_index(p,px);
    distances[tot] = dist_from_solution(i,px,pI) * trace[i];
    ind[tot] = i;
  }
  
  sort_double (tot, distances, ind);
  i_extracted = extract_next_point(ind, dim+1, q, seed);

  free(distances);
  free(ind);
  return i_extracted;
}

/* Esegue l'algoritmo greedy per generare una soluzione. E' influenzato sia
   dalla memoria (la traccia), sia dalla casualita'.
   Sceglie il primo punto a caso e i successivi basandosi su traccia e
   casualita' (con l'apposita funzione)
*/
void randomized_greedy (data_t *pI, solution_t *px, double q, double *trace, long *seed) {
   point i;
   while (px->card_x < pI->k) {
     i = best_additional_point_with_trace(px, pI, q, trace, seed);
     move_point_in(i,px,pI);
   }
}


/* Funzione che inizializza ogni elemento della traccia al valore indicato */
double* init_trace (size_t n, double value) {
  int i;
  double *trace = double_alloc(n);
  for (i = 0; i < n; i++) {
    trace[i] = value;
  }
  return trace;
}

/* Funzione di aggiornamento della traccia: favorisce i punti all'interno
   della soluzione per favorire una intensificazione
*/
void intensify_trace (double *trace, solution_t *px, int dimension, double obl) {
  int i;
  for (i = 1; i < dimension; i++) {
    if (px->in_x[i]) {
      trace[i] = 1 - (1 - obl)*(1 - trace[i]);
    }
    else {
      trace[i] = trace[i]*(1 - obl);
    }
  }
}

/* Funzione di aggiornamento della traccia: sfavorisce i punti all'interno
   della soluzione per favorire una diversificazione
*/
void diversify_trace (double *trace, solution_t *px, int dimension, double obl) {
  int i;
  for (i = 1; i < dimension; i++) {
    if (px->in_x[i]) {
      trace[i] = trace[i]*(1 - obl);
    }
    else {
      trace[i] = 1 - (1 - obl)*(1 - trace[i]);
    }
  }
}

/* Esegue l'euristica Ant, con il numero specificato di generazioni ed agenti.
   Richiede inoltre di specificare il parametro di oblio per l'aggiornamento
   della traccia, il parametro q che viene utilizzato per gestire la scelta
   dell'elemento all'interno dell'algoritmo greedy e il seed per le scelte casuali */
void ant_colony (data_t *pI, solution_t *px, int generations, int ants, double q, double obl, long *seed) {
  int g, a, dim;
  double *local_trace, *global_trace;
  solution_t curr_best, local;

  dim = pI->n;
  local_trace = double_alloc(dim + 1);
  global_trace = init_trace(dim + 1, 0.5);

  create_solution (dim, &local);
  create_solution (dim, &curr_best);

  randomized_greedy(pI, px, 0, global_trace, seed);
  intensify_trace(global_trace, px, dim+1, obl);

  for (g = 0; g < generations; g++) {
    memcpy(local_trace, global_trace, (sizeof(double)*(dim+1)));

    for (a = 0; a < ants; a++) {
      randomized_greedy(pI, &local, q, local_trace, seed);
      diversify_trace(local_trace, &local, dim+1, obl);

      if (local.f > curr_best.f) {
        copy_solution(&local, &curr_best);
      }
      clean_solution(&local);
    }
    intensify_trace(global_trace, &curr_best, dim+1, obl);

    if (curr_best.f > px->f) {
      copy_solution(&curr_best, px);
    }
    clean_solution(&curr_best);
  }

  destroy_solution(&local);
  destroy_solution(&curr_best);
  free(global_trace);
  free(local_trace);
}