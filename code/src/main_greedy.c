#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "ant.h"
#include "grasp.h"
#include "partition.h"
#include "semigreedy.h"

void parse_command_line (int argc, char *argv[], char *data_file, char *param, double *palpha,
  int *piterTot, int *ants, double *obl, long *pseme);

void get_istance_name (char *long_name, char *istance, int namelen);

int main (int argc, char *argv[])
{
  int namelen = 13; //da modificare in base alla lunghezza del nome delle istanze
  char data_file[NAME_LENGTH];
  char istance_name[namelen + 1];
  data_t I;
  solution_t x;
  char param[NAME_LENGTH];
  double alpha;
  int iterTot;
  long seme;
  double obl;
  int ants;
  time_t inizio, fine;
  double tempo;

  parse_command_line(argc,argv,data_file,param,&alpha,&iterTot,&ants,&obl,&seme);
  
  load_data(data_file,&I);
  //print_data(&I);

  create_solution(I.n,&x);

  inizio = clock();
  if (strcmp(param,"-g") == 0)
    greedy(&I,&x);
  else if (strcmp(param,"-gbs") == 0)
    greedy_bestsum(&I,&x);
  else if (strcmp(param,"-gbp") == 0)
    greedy_bestpair(&I,&x);
  else if (strcmp(param,"-gta") == 0)
    greedy_tryall(&I,&x);
  else if (strcmp(param,"-sg") == 0)
    semigreedy(&I,&x,alpha,&seme);
  else if (strcmp(param,"-gv") == 0)
    grasp_RCL_value(&I,&x,alpha,iterTot,&seme);
  else if (strcmp(param,"-gc") == 0)
    grasp_RCL_cardinality(&I,&x,ants,iterTot,&seme);
  else if (strcmp(param,"-a") == 0)
    ant_colony(&I,&x,iterTot,ants,alpha,obl,&seme);
  fine = clock();
  tempo = (double) (fine - inizio) / CLOCKS_PER_SEC;

  get_istance_name(data_file, istance_name, namelen);
  printf("%s ", istance_name);
  printf("%10.6lf ",tempo);
  print_solution(&x);
  printf("\n");

  destroy_solution(&x);
  destroy_data(&I);

  return EXIT_SUCCESS;
}


void print_usage (char *command)
{
  fprintf(stderr,"Use: %s [datafile] [-g|-gbs|-gbp|-gta|-sg|-gv|-gc|-a]\n",command);
  fprintf(stderr,"datafile:                    name and path of data file\n");
  fprintf(stderr,"-g:                          basic greedy heuristic\n");
  fprintf(stderr,"-gbs:                        greedy heuristic starting from point with the largest distance sum\n");
  fprintf(stderr,"-gbp:                        greedy heuristic starting from two farthest points\n");
  fprintf(stderr,"-gta:                        greedy heuristic starting from each point\n");
  fprintf(stderr,"-sg alpha seed:              semigreedy heuristic\n");
  fprintf(stderr,"-gv alpha iterTot seed:      GRASP heuristic (RCL by value)\n");
  fprintf(stderr,"-gc card iterTot seed:       GRASP heuristic (RCL by cardinality)\n");
  fprintf(stderr,"-a gen ants q oblivion seed: ant colony heuristic\n");
}


void parse_command_line (int argc, char *argv[], char *data_file, char *param, double *palpha,
  int *piterTot, int *ants, double *obl, long *pseme)
{
  if ((argc < 2) || (argc > 8))
  {
    print_usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  strcpy(data_file,argv[1]);
  strcpy(param,"-g");
  if (argc >= 3)
  {
    if ( (strcmp(argv[2],"-g") != 0) && (strcmp(argv[2],"-gbs") != 0) &&
         (strcmp(argv[2],"-gbp") != 0) && (strcmp(argv[2],"-gta") != 0) &&
         (strcmp(argv[2],"-sg") != 0) && (strcmp(argv[2],"-gv") != 0) &&
         (strcmp(argv[2],"-gc") != 0) && (strcmp(argv[2],"-a") != 0))
    {
      print_usage(argv[0]);
      exit(EXIT_FAILURE);
    }
    strcpy(param,argv[2]);
    //semigreedy
    if (strcmp(argv[2],"-sg") == 0) 
    {   
      if (argc != 5)
      {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
      }
      sscanf(argv[3],"%lf",palpha);
      sscanf(argv[4],"%ld",pseme);
    }
    //GRASP RCL value
    if (strcmp(argv[2],"-gv") == 0) 
    {   
      if (argc != 6)
      {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
      }
      sscanf(argv[3],"%lf",palpha);      
      sscanf(argv[4],"%ld",piterTot);
      sscanf(argv[4],"%ld",pseme);
    }
    //GRASP RCL cardinality
    if (strcmp(argv[2],"-gc") == 0) 
    {   
      if (argc != 6)
      {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
      }
      sscanf(argv[3],"%ld",ants);      
      sscanf(argv[4],"%ld",piterTot);
      sscanf(argv[4],"%ld",pseme);
    }
    //Ant
    if (strcmp(argv[2],"-a") == 0) 
    {   
      if (argc != 8)
      {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
      }
      sscanf(argv[3],"%d",piterTot);
      sscanf(argv[4],"%d",ants);
      sscanf(argv[5],"%lf",palpha);
      sscanf(argv[6],"%lf",obl);
      sscanf(argv[7],"%ld",pseme);
    }
  }
}

/* Estrae il solo nome dell'istanza dal pathname, in modo da stampare la soluzione
   in maniera più fancy. Attenzione a gestire correttamente namelen e ad inizializzare
   un array per il nome dell'istanza di dimensione namelen+1 (per il terminatore)
*/

void get_istance_name (char *long_name, char *istance, int namelen) {
  int len = strlen(long_name);

  memcpy(istance, (long_name + len - namelen), namelen*sizeof(char));
  istance[namelen] = '\0';
}