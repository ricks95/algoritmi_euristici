\documentclass{article}

\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage{amsmath}
\usepackage{float}
\usepackage{graphicx}
\usepackage[dvipsnames]{xcolor}
\usepackage{listings}

\title{Laboratorio sulle Metaeuristiche Costruttive}

\begin{document}
	\lstset{ %settings generali per listings
		language=C,
		morekeywords={data\_t,solution\_t},
		keywordstyle=\color{NavyBlue}\bfseries,
		basicstyle=\small,
		tabsize=2,
		showstringspaces=false,
		numbers=left,
		numberstyle=\tiny,
		numbersep=2pt,
		emphstyle=\color{Green}\bfseries}
	
	\lstset{ %keyword addizionali
		emph={grasp\_RCL\_value,best\_random\_additional\_point\_value,partition\_by\_value,grasp\_RCL\_card,best\_random\_additional\_point\_card,partition\_by\_card,ant\_colony,double\_alloc,init\_trace,create\_solution,randomized\_greedy,intensify\_trace,memcpy,diversify\_trace,copy\_solution,clean\_solution,destroy\_solution,free,best\_additional\_point\_with\_trace,move\_point\_in,extract\_next\_point,rand\_int,int\_alloc,first\_point\_out,end\_point\_list,next\_point,get\_index,dist\_from\_solution,sort\_double,scambia\_double,scambia}
	}
	
	\newfloat{lstfloat}{htbp}{lop} %float per i listing di codice
	\floatname{lstfloat}{Listing}
	\floatstyle{ruled}
	\def\lstfloatautorefname{Listing}
	
	\maketitle
	\section{Introduzione}
	Questo laboratorio presenta due diverse metaeuristiche costruttive applicate al Maximum Diversity Problem (MDP). Le metaeuristiche costruttive vengono in genere utilizzate per migliorare i risultati delle euristiche costruttive base, tramite, ad esempio, la ripetizione dell'algoritmo modificando parametri come la funzione di scelta al fine di ottenere più soluzioni diverse da confrontare. Ulteriori elementi aggiuntivi possono essere l'utilizzo di scelte casuali (come vedremo nel GRASP) e di memoria (come vedremo nell'Ant System, unitamente a scelte casuali). Le metaeuristiche presentate all'interno di questo laboratorio si avvalgono anche dell'utilizzo di euristiche di scambio per migliorare i risultati, ma questa parte verrà momentaneamente evitata per concentrarsi invece sulla metaeuristica costruttiva pura e semplice.
	\section{GRASP}
	Greedy Randomized Adaptive Search Procedure, o GRASP, è una variante più sofisticata della semplice euristica costruttiva semigreedy (che basa la scelta dell'elemento successivo da includere su una distribuzione di probabilità che favorisce gli elementi migliori fra quelli esterni alla soluzione parziale fino a quel momento ottenuta).
	Analizzando il nome GRASP, si ottiene che:
	\begin{itemize}
		\item \textit{Greedy} indica che utilizza un'euristica costruttiva base;
		\item \textit{Randomized} indica che l'euristica base compie passi casuali;
		\item \textit{Adaptive} indica che il criterio di scelta $\varphi_{A}(i,x)$ è adattivo, ovvero dipende anche dalla soluzione $x$ (anche se non è strettamente necessario);
		\item \textit{Search} indica che vengono eseguite anche euristiche di scambio accanto alle costruttive.
	\end{itemize}
	Le euristiche di scambio ausiliarie portano ad un buon miglioramento nei risultati, ma come accennato nell'introduzione verranno affrontate in seguito, pertanto non sarà presente una loro trattazione in questo laboratorio.
	Di seguito viene presentato lo schema base di GRASP. Questo schema fa riferimento ad un problema di minimizzazione, per adattarlo ad un problema di massimizzazione è sufficiente partire da un valore di $f$ pari a $-\infty$ e sostituire la soluzione se il valore di quella appena trovata è maggiore (e non minore) della migliore nota. L'algoritmo presentato esegue $l$ iterazioni e per ciascuna costruisce una soluzione tramite euristica costruttiva con passi casuali. Al termine di ogni iterazione esegue un'euristica di scambio sulla soluzione appena costruita e controlla se questa supera o meno la miglior soluzione nota finora. Una delle possibilità per gestire la componente casuale è costruire una Restricted Candidate List (RCL), che contiene un certo numero di elementi fra tutti quelli esterni alla soluzione: a questi viene assegnata probabilità uniforme di estrazione, agli altri zero. Per stabilire le dimensioni della RCL si utilizza un parametro utente ($\mu$) e si può procedere per cardinalità (i migliori $\mu$ elementi in base al valore $\varphi$) oppure per valore (gli elementi con valore compreso fra $\varphi_{min}$ e $\varphi_{min}+\mu(\varphi_{max}-\varphi_{min})$).
	\begin{algorithm}
	\caption{GRASP}
	\begin{algorithmic}[1]
		\Procedure{GRASP}{$I$}
		\State $x^{*}:=\emptyset;$
		\State $f^{*}:=\infty;$\Comment{Miglior soluzione trovata finora}
		\For{$i:=1\: \textbf{to}\: l$}
			\State $x:=\emptyset;$
			\While{$Ext(x)\neq\emptyset$}\Comment{Euristica con passi casuali}
				\State $\varphi_{i}:=\varphi_{A}(i,x)\forall i\in Ext(x);$
				\State $L:=Sort(Ext(x),\varphi);$\Comment{Ordino Ext(x) in base a $\varphi$}
				\State $\pi:=AssignProb(L,\mu);$\Comment{Oppure costruisco RCL}
				\State $j:=RandExtract(Ext(x),\pi);$
				\State $x:=x\cup \{i\};$
			\EndWhile
			\State $x:=Search(x);$\Comment{Euristica di scambio}
			\If{$x\in X\:\textbf{and}\:f(x)<f^{*}$}
				\State $x^{*}:=x;$
				\State $f^{*}:=f(x);$
			\EndIf
		\EndFor
		\State \textbf{return} $(x^{*},f^{*});$
		\EndProcedure
	\end{algorithmic}
	\end{algorithm}
	\subsection{Codice}
	In questa sezione viene presentato il codice per realizzare l'algoritmo GRASP che risolva MDP. Come già accennato, non viene implementata la procedura di scambio, che verrà trattata in seguito. Inoltre, il punto di partenza è il codice del laboratorio precedente, quindi si farà affidamento sulle funzioni già disponibili per molte delle operazioni.
	La versione di GRASP presentata utilizza la RCL, con le due modalità (per valore e per cardinalità) richiamate nella precedente sezione. Entrambe le modalità utilizzano una procedura che seleziona il miglior punto esterno alla soluzione, la differenza sta nella costruzione della RCL.
	\subsubsection{RCL per Valore}
	Di seguito viene esposto il codice per realizzare GRASP con RCL per valore. Il primo estratto di codice è la procedura che gestisce le diverse iterazioni e richiama la funzione per la selezione del miglior punto esterno alla soluzione. Al termine di ogni iterazione avviene il confronto con la miglior soluzione nota, che viene rimpiazzata in caso sia stato ottenuto un miglioramento.
	
	\begin{lstfloat}[H]
	\caption{GRASP - RCL per Valore}
	\begin{lstlisting}
	void grasp_RCL_value (data_t *pI, solution_t *px, double alpha,
	 int iterTot, long *pidum) {
		int i, iter;
		solution_t xCurr;
	
		create_solution(pI->n,&xCurr);
		copy_solution(px,&xCurr);
	
		for (iter = 0; iter < iterTot; iter++) {
			while (xCurr.card_x < pI->k) {
				i = best_random_additional_point_value(&xCurr,pI,alpha,pidum);
				move_point_in(i,&xCurr,pI);
			}
			if (xCurr.f > px->f) {
			copy_solution(&xCurr,px);
			}
			clean_solution(&xCurr);
		}
		destroy_solution(&xCurr);
	}
	\end{lstlisting}
	\end{lstfloat}
	Per analizzare il tempo di esecuzione, fissiamo $N$ come il numero di punti totali e $K$ come il numero di punti nella soluzione; inoltre sarà $X$ il costo della procedura \textbf{best\_random\_additional\_point\_value}, descritto in dettaglio in seguito. Il tempo di esecuzione della procedura sarà $\Theta(K*X)$, con il numero di iterazioni che viene incluso nel $\Theta$ e le procedure che inseriscono il punto e copiano la soluzione aventi un tempo di esecuzione lineare.

	Più interessante è la procedura \textbf{best\_random\_additional\_point\_value}, che si occupa di selezionare il miglior punto esterno alla soluzione fino ad ora costruita. Per farlo scorre la lista di tutti i punti esterni e ne calcola la distanza dalla soluzione corrente, segnando in parallelo anche gli indici dei punti. Tramite \textbf{partition\_by\_value} questi due array paralleli vengono riordinati insieme in modo da portare in cima i valori più significativi. A decidere quanti siano questi valori è il parametro alpha, che viene utilizzato per calcolare quali valori di distanza considerare tramite la formula descritta nella sezione precedente. Una volta ottenuto il numero di elementi da considerare, come risultato della procedura, viene effettuata una estrazione casuale con probabilità uniforme sulla RCL e l'elemento estratto viene aggiunto in soluzione.
	
	\begin{lstfloat}[H]
	\caption{Best Additional Point - Valore}
	\begin{lstlisting}
	int best_random_additional_point_value (solution_t *px,
	 data_t *pI, double alpha, long *pidum) {
		point p;
		int *d;
		int i, i_max;
		int *ind;
		int num, tot;

		d = int_alloc(pI->n+1);
		ind = int_alloc(pI->n+1);

		tot = 0;
		for (p = first_point_out(px); !end_point_list(p,px);
		 p = next_point(p,px)) {
			tot++;
			i = get_index(p,px);
			d[tot] = dist_from_solution(i,px,pI);
			ind[tot] = i;
		}
	
		num = partition_by_value(tot,d,ind,alpha);
	
		i_max = ind[rand_int(1,num,pidum)];
	
		free(d);
		free(ind);
		return i_max;
	}
	\end{lstlisting}
	\end{lstfloat}
	Il tempo di esecuzione di questa procedura deve considerare principalmente il ciclo for, che scorre su tutti i punti esterni alla soluzione corrente, congiuntamente alla procedura \textbf{dist\_from\_solution} che invece scorre su tutti i punti compresi nella soluzione. Il tempo risulterà essere circa $\Theta(N*K)$, con $N$ e $K$ definiti sopra.
	Alla luce di questo e delle considerazioni effettuate in precedenza, il tempo teorico totale per GRASP per valore risulta $\Theta(K^{2}*N)$. Bisogna tenere presente che il valore di $K$ cresce durante l'esecuzione, perchè la soluzione viene costruita a partire da zero, quindi i passaggi sulla soluzione corrente saranno più veloci all'inizio e più lenti alla fine. Un ragionamento simile ma opposto si può applicare a $N$, poichè i punti esterni vengono gradualmente inclusi nella soluzione.
	
	\subsubsection{RCL per Cardinalità}
	Di seguito viene esposto il codice per realizzare GRASP con RCL per cardinalità. La prima procedura presentata è di fatto uguale alla precedente, a meno della funzione \textbf{best\_random\_additional\_point\_card} che gestisce in maniera differente la RCL (per cardinalità, appunto).
	
	\begin{lstfloat}[H]
	\caption{GRASP - RCL per Cardinalità}
	\begin{lstlisting}
	void grasp_RCL_cardinality (data_t *pI, solution_t *px,
	 int card, int iterTot, long *pidum) {
		int i, iter;
		solution_t xCurr;
	
		create_solution(pI->n,&xCurr);
		copy_solution(px,&xCurr);

		for (iter = 0; iter < iterTot; iter++) {
			while (xCurr.card_x < pI->k) {
				i = best_random_additional_point_card(&xCurr,pI,card,pidum);
				move_point_in(i,&xCurr,pI);
			}
			if (xCurr.f > px->f) {
				copy_solution(&xCurr,px);
			}
			clean_solution(&xCurr);
		}
		destroy_solution(&xCurr);
	}
	\end{lstlisting}
	\end{lstfloat}
	Per analizzare il tempo di esecuzione, fissiamo $N$ come il numero di punti totali e $K$ come il numero di punti nella soluzione; inoltre sarà $X$ il costo della procedura \textbf{best\_random\_additional\_point\_card}, descritto in dettaglio in seguito. Il tempo di esecuzione della procedura sarà $\Theta(K*X)$, con il numero di iterazioni che viene incluso nel $\Theta$ e le procedure che inseriscono il punto e copiano la soluzione aventi un tempo di esecuzione lineare.

	Come prima, analizziamo più in dettaglio la procedura che si occupa della selezione del prossimo punto, \textbf{best\_random\_additional\_point\_card}. Questa volta l'utente specifica la cardinalità desiderata per la RCL e \textbf{partition\_by\_card} si occupa di ordinare gli array di indici e distanze, assicurandosi che i primi $card$ punti siano i più distanti disponibili. Viene poi effettuata un estrazione uniforme su questi punti e il punto estratto viene aggiunto in soluzione.
	
	\begin{lstfloat}[H]
	\caption{Best Additional Point - Cardinalità}
	\begin{lstlisting}
	int best_random_additional_point_card (solution_t *px,
	 data_t *pI, int card, long *pidum) {
		point p;
		int *d;
		int i, i_max, dim;
		int *ind;
		int num, tot;
	
		dim = pI->n+1;
		d = int_alloc(dim);
		ind = int_alloc(dim);
	
		tot = 0;
		for (p = first_point_out(px); !end_point_list(p,px);
		 p = next_point(p,px)) {
			tot++;
			i = get_index(p,px);
			d[tot] = dist_from_solution(i,px,pI);
			ind[tot] = i;
		}
	
		partition_by_card(dim, d, ind, card);
	
		i_max = ind[rand_int(1,card,pidum)];
	
		free(d);
		free(ind);
		return i_max;
	}
	\end{lstlisting}
	\end{lstfloat}
	Il tempo di esecuzione di questa procedura deve considerare principalmente il ciclo for, che scorre su tutti i punti esterni alla soluzione corrente, congiuntamente alla procedura \textbf{dist\_from\_solution} che invece scorre su tutti i punti compresi nella soluzione. Il tempo risulterà essere circa $\Theta(N*K)$, con $N$ e $K$ definiti sopra. Anche la procedura \textbf{partition\_by\_card} ha tempo di esecuzione $\Theta(N*K)$.
	Alla luce di questo e delle considerazioni effettuate in precedenza, il tempo teorico totale per GRASP per cardinalità risulta $\Theta(K^{2}*N)$. Bisogna tenere presente che il valore di $K$ cresce durante l'esecuzione, perchè la soluzione viene costruita a partire da zero, quindi i passaggi sulla soluzione corrente saranno più veloci all'inizio e più lenti alla fine. Un ragionamento simile ma opposto si può applicare a $N$, poichè i punti esterni vengono gradualmente inclusi nella soluzione.
	
	\subsection{Sperimentazione}
	La sperimentazione è stata effettuata provando diverse combinazioni di parametri. In particolare sono stati utilizzati i seguenti valori:
	\begin{itemize}
		\item {$2, 4, 8$ per la cardinalità;}
		\item {$0.05, 0.10, 0.15$ per le percentuali del valore;}
		\item {$10, 20, 30$ per le iterazioni.}
	\end{itemize}
	
	La scelta di valori così bassi per cardinalità e valore deriva dall'osservazione che in questa versione semplificata di GRASP, senza la procedura di scambio, utilizzare valori più alti porta ad un significativo peggioramento dei risultati, dal momento che eventuali scelte poco felici non vengono corrette dalla procedura di scambio. In generale, con questa versione del GRASP i risultati migliori sono stati rilevati con bassi valori della dimensione della RCL e maggior numero di iterazioni.
	Riportiamo ora i grafici per Valore e poi per Cardinalità. I grafici relativi al tempo prendono in esame tutte le istanze, quelli SQD invece sono stati eseguiti sul gruppo di istanze di valore 200, dal momento che ha senso utilizzare SQD su istanze di ugual cardinalità. Poichè i dati a disposizione sono piuttosto limitati in tal senso, questi ultimi grafici non avranno estrema rilevanza.
	\begin{figure}[H]
		\caption{Tempo per GRASP - Valore}
		\includegraphics[width=\textwidth]{../img/gv_time.pdf}
	\end{figure}
	\begin{figure}[H]
		\caption{SQD per GRASP - Valore}
		\includegraphics[width=\textwidth]{../img/gv_sqd.pdf}
	\end{figure}

	\begin{figure}[H]
		\caption{Tempo per GRASP - Cardinalità}
		\includegraphics[width=\textwidth]{../img/gc_time.pdf}
	\end{figure}
	\begin{figure}[H]
		\caption{SQD per GRASP - Cardinalità}
		\includegraphics[width=\textwidth]{../img/gc_sqd.pdf}
	\end{figure}
	
	\section{Ant System}
	Ant System (AS) nasce ispirandosi al comportamento sociale delle formiche. In particolare, vuole ricalcare la \textit{stigmergia}, ovvero la comunicazione indiretta fra agenti che sono stimolati e guidati dalle azioni proprie ed altrui. In natura, gli agenti sono le formiche che seguono le tracce lasciate dai propri simili per trovare il percorso migliore fino alla fonte di cibo, nel nostro caso invece ciascun agente è un'applicazione dell'euristica costruttiva base. L'agente lascia una traccia sui dati che dipende dalla soluzione prodotta e compie le proprie scelte utilizzando la traccia lasciata dagli altri agenti. AS, quindi, fa uso della memoria, nella forma di una traccia che viene aggiornata ad ogni passaggio di un agente. Oltre a ciò, AS utilizza anche la casualità, presente all'interno dell'euristica costruttiva.
	\begin{algorithm}
	\caption{Ant System}
	\begin{algorithmic}[1]
		\Procedure{AntSystem}{$I$}
		\State $x^{*}:=\emptyset;$
		\State $f^{*}:=\infty;$\Comment{Miglior soluzione trovata finora}
		\State $InizializzaTraccia(\tau_{A});$
		\For{$i:=1\: \textbf{to}\: l$}\Comment{Iterazioni}
			\For{$g:=1\: \textbf{to}\: f$}\Comment{Formiche/agenti}
				\State $x:=A(I,\tau_{A});$\Comment{Euristica con passi casuali e traccia}
				\State $x:=Search(x);$\Comment{Euristica di scambio}
				\If{$x\in X\:\textbf{and}\:f(x)<f^{*}$}
					\State $x^{*}:=x;$\Comment{Aggiornamento soluzione migliore}
					\State $f^{*}:=f(x);$
				\EndIf
				\State $\tau_{A}:=AggLocale(\tau_{A},x,\rho);$
				\State $\overline{X}^{[l]}:=AggSolInfluenti(\overline{X}^{[l]},x);$
			\EndFor
			\State $\tau_{A}:=AggGlobale(\tau_{A},\overline{X}^{[l]},\rho);$
		\EndFor
		\State \textbf{return} $(x^{*},f^{*});$
		\EndProcedure
	\end{algorithmic}
	\end{algorithm}
	\subsection{Codice}
	In questa sezione viene presentato il codice per l'implementazione di Ant System per la risoluzione di MDP. Come per l'algoritmo precedente, anche in questo caso non verrà implementata la procedura di scambio, lasciata ad un prossimo laboratorio, e il punto di partenza è il codice del precedente laboratorio.
	\subsubsection{Schema Generale}
	L'implementazione di Ant per MDP segue lo schema generale descritto nella sezione precedente. Si è scelto di implementare l'algoritmo greedy randomizzato e la gestione della traccia come funzioni per una maggiore pulizia del codice. Queste funzioni, unitamente ad alcune di utility, saranno descritte in seguito. La traccia viene inizializzata ad un valore di $0.5$ (riga 9) per tutti gli elementi, pertanto la prima esecuzione della procedura greedy randomizzata non verrà influenzata dalla traccia. All'inizio di ogni generazione, la traccia locale viene impostata uguale a quella globale (riga 18), che incoraggia una ricerca nei pressi delle soluzioni migliori. Ciascuno degli agenti eseguirà la procedura greedy randomizzata e influenzata dalla traccia, che verrà poi aggiornata per favorire, questa volta, una diversificazione rispetto alla soluzione appena trovata (righe 21-22). Si tiene traccia della miglior soluzione per la generazione corrente e, ovviamente, della migliore globale. Al termine della procedura viene liberata la memoria allocata per soluzioni e puntatori.

	\begin{lstfloat}[H]
	\caption{Schema Generale AS}
	\begin{lstlisting}
	void ant_colony (data_t *pI, solution_t *px, int generations,
	 int ants, double q, double obl, long *seed) {
		int g, a, dim;
		double *local_trace, *global_trace;
		solution_t curr_best, local;
		
		dim = pI->n;
		local_trace = double_alloc(dim + 1);
		global_trace = init_trace(dim + 1, 0.5);

		create_solution (dim, &local);
		create_solution (dim, &curr_best);

		randomized_greedy(pI, px, 0, global_trace, seed);
		intensify_trace(global_trace, px, dim+1, obl);

		for (g = 0; g < generations; g++) {
			memcpy(local_trace, global_trace, (sizeof(double)*(dim+1)));

			for (a = 0; a < ants; a++) {
				randomized_greedy(pI, &local, q, local_trace, seed);
				diversify_trace(local_trace, &local, dim+1, obl);

				if (local.f > curr_best.f) {
					copy_solution(&local, &curr_best);
				}
				clean_solution(&local);
			}
			intensify_trace(global_trace, &curr_best, dim+1, obl);

			if (curr_best.f > px->f) {
				copy_solution(&curr_best, px);
			}
			clean_solution(&curr_best);
		}

		destroy_solution(&local);
		destroy_solution(&curr_best);
		free(global_trace);
		free(local_trace);
	}
	\end{lstlisting}
	\end{lstfloat}
	Similmente alle precedenti procedure analizzate per GRASP consideriamo $N$ il numero di punti esterni alla soluzione e $K$ quello dei punti interni ad essa.
	Il costo di AS è $\Theta(K^{2}*N)$, derivato dal costo del greedy randomizzato analizzato sotto. La quantità di agenti ed il numero di generazioni chiaramente influenzeranno il tempo di calcolo, ma per l'analisi teorica vengono assorbiti dal $\Theta$. Le altre procedure hanno costi inferiori o trascurabili rispetto al greedy randomizzato che rappresenta la parte più onerosa computazionalmente.

	\subsubsection{Greedy Randomizzato}
	La procedura greedy randomizzata è di per sè molto semplice e simile a quella descritta nel primo laboratorio (nel nostro caso con aggiunta di casualità e traccia). Si itera fino ad avere una soluzione di cardinalità k scegliendo di volta in volta il punto migliore da aggiungere alla soluzione.

	\begin{lstfloat}[H]
	\caption{Greedy Randomizzato}
	\begin{lstlisting}
	void randomized_greedy (data_t *pI, solution_t *px, double q,
	 double *trace, long *seed) {
		point i;
		while (px->card_x < pI->k) {
			i = best_additional_point_with_trace(px, pI, q, trace, seed);
			move_point_in(i,px,pI);
		}
	}
	\end{lstlisting}
	\end{lstfloat}
	Il costo di questa procedura risulta essere $\Theta(K*X)$, dove $X$ è il costo di \textbf{best\_random\_additional\_point\_with\_trace}, esaminato sotto.

	Merita più attenzione la procedura \textbf{best\_additional\_point\_with\_trace} che si occupa della selezione del punto migliore. Dopo una fase di inizializzazione delle aree di memoria necessarie, la procedura itera su tutti i punti esterni alla soluzione, calcolando per ciascuno il prodotto $distanza*traccia$ e salvandolo in un array. Parallelamente, viene salvato anche l'indice di ciascun punto. Tramite la procedura \textbf{sort\_double} (descritta nella sezione \ref{ut}) questi due array vengono ordinati in parallelo, in modo da ottenere gli indici in ordine decrescente rispetto alla loro bontà per la soluzione.

	\begin{lstfloat}[H]
	\caption{Procedura per la selezione del prossimo punto}
	\begin{lstlisting}
	int best_additional_point_with_trace (solution_t *px,
	 data_t *pI, double q, double *trace, long *seed) {
		point p;
		double *distances;
		int i, i_extracted, num, tot, dim;
		int *ind;
	
		dim = (px->card_N) - (px->card_x);
		distances = double_alloc(dim+1);
		ind = int_alloc(dim+1);
		tot = 0;
	
		for (p = first_point_out(px); !end_point_list(p,px);
		 p = next_point(p,px)) {
			tot++;
			i = get_index(p,px);
			distances[tot] = dist_from_solution(i,px,pI) * trace[i];
			ind[tot] = i;
		}
	
		sort_double (tot, distances, ind);
		i_extracted = extract_next_point(ind, dim+1, q, seed);
	
		free(distances);
		free(ind);
		return i_extracted;
	}
	\end{lstlisting}
	\end{lstfloat}
	Le considerazioni sul costo in termini di tempo si concentrano principalmente sul ciclo for. In maniera simile a quanto detto per le corrispondenti procedure in GRASP, il tempo di calcolo è rappresentato principalmente dal blocco del ciclo for, per un $\Theta(N*K)$. Questo tempo risulta simile a quello della procedura \textbf{sort\_double}, per le considerazioni riportate sotto.

	A questo punto avviene l'estrazione casuale del punto tramite la funzione \textbf{extract\_next\_point}, che restituisce il miglior punto con probabilità $1-q$ oppure ripete il check sul punto successivo nella lista.

	\begin{lstfloat}[H]
	\caption{Estrazione punto casuale}
	\begin{lstlisting}
	int extract_next_point (int *indices, int dimension,
	 double q, long *seed) {
		int rand, i;
		double check;

		for (i = 1; i < (dimension - 1); i++) {
			rand = rand_int(1, 100, seed);
			check = rand / 100.0;
			if (check <= (1-q)) {
				return indices[i];
			}
		}
		return indices[i-1];
	}
	\end{lstlisting}
	\end{lstfloat}
	Il tempo di calcolo di questa procedura è lineare nel numero di punti esterni alla soluzione corrente, quindi $\Theta(N)$.

	\subsubsection{Gestione Traccia}
	Le procedure per la gestione della traccia utilizzano il parametro di oblio, che decide quanta importanza deve avere la memoria rispetto alla soluzione corrente $M$: se l'oblio è alto, gran parte del peso della traccia deriverà dalla soluzione appena costruita, viceversa la soluzione con cui si aggiorna la traccia la influenzerà in maniera ridotta. La formula utilizzata per l'aggiornamento locale (che è una diversificazione) è (con $N$ insieme di tutti gli elementi):
	\[\tau_{i}^{(l)}:=\begin{cases}
	\tau_{i}^{(l-1)}(1-\rho) & se\:i\in M\\
	1-(1-\rho)(1-\tau_{i}^{(l-1)}) & se\:i\in N\setminus M)
	\end{cases} \]
	\begin{lstfloat}[H]
	\caption{Aggiornamento Locale - Diversificazione}
	\begin{lstlisting}
	void diversify_trace (double *trace, solution_t *px,
	 int dimension, double obl) {
		int i;
		for (i = 1; i < dimension; i++) {
			if (px->in_x[i]) {
				trace[i] = trace[i]*(1 - obl);
			}
			else {
				trace[i] = 1 - (1 - obl)*(1 - trace[i]);
			}
		}
	}
	\end{lstlisting}
	\end{lstfloat}

	La formula utilizzata per l'aggiornamento globale (che è un'intensificazione) è simile alla precedente (con i casi invertiti), questa volta viene utilizzata $M^{(l)}$ che è la miglior soluzione trovata nella generazione appena terminata:
	\[\tau_{i}:=\begin{cases}
	1-(1-\rho)(1-\tau_{i}) & se\:i\in M^{(l)}\\
	\tau_{i}(1-\rho) & se\:i\in N\setminus M^{(l)}
	\end{cases} \]
	\begin{lstfloat}[H]
	\caption{Aggiornamento Globale - Intensificazione}
	\begin{lstlisting}
	void intensify_trace (double *trace, solution_t *px,
	 int dimension, double obl) {
		int i;
		for (i = 1; i < dimension; i++) {
			if (px->in_x[i]) {
				trace[i] = 1 - (1 - obl)*(1 - trace[i]);
			}
			else {
				trace[i] = trace[i]*(1 - obl);
			}
		}
	}
	\end{lstlisting}
	\end{lstfloat}
	
	Per ultima presentiamo una semplice procedura che inizializza una traccia al valore passato come parametro.
	\begin{lstfloat}[H]
	\caption{Inizializzazione}
	\begin{lstlisting}
	double* init_trace (size_t n, double value) {
		int i;
		double *trace = double_alloc(n);
		for (i = 0; i < n; i++) {
			trace[i] = value;
		}
		return trace;
	}
	\end{lstlisting}
	\end{lstfloat}
	Tutte le procedure riguardanti l'aggiornamento della traccia hanno un tempo di esecuzione lineare nel numero di punti dell'istanza, poichè effettuano una singola passata sull'array che gestisce la traccia, quindi $\Theta(N)$.

	\subsubsection{Utility}\label{ut}
	In questa sezione compaiono alcune procedure ausiliarie che vengono utilizzate all'interno dell'euristica. Per quanto non siano nulla di particolare vengono riportate per completezza.
	La procedura \textbf{double\_alloc} si occupa di inizializzare un array di double di dimensione indicata (è necessario aggiungerla per occuparsi delle tracce).
	\begin{lstfloat}[H]
	\caption{Double Alloc}
	\begin{lstlisting}
	double *double_alloc (int n) {
		double *v = (double *) calloc(n, sizeof(double));
		if (v == NULL) {
			fprintf(stderr,"Not enough memory to allocate
			 a vector of %d double!",n);
			exit(EXIT_FAILURE);
		}
		return v;
	}
	\end{lstlisting}
	\end{lstfloat}

	La procedura \textbf{scambia\_double} si occupa semplicemente di scambiare due double.
	\begin{lstfloat}[H]
	\caption{Scambia Double}
	\begin{lstlisting}
	void scambia_double (double *pa, double *pb) {
		double temp;
	
		temp = *pa;
		*pa = *pb;
		*pb = temp;
	}
	\end{lstlisting}
	\end{lstfloat}

	Per finire, la procedura \textbf{sort\_double} prende in input un vettore di double $D$ e un corrispondente vettore di int e li ordina in parallelo, in base ai valori in $D$. Nell'algoritmo presentato viene utilizzato per ordinare gli indici dei punti in base al valore della distanza dalla soluzione corrente.
	\begin{lstfloat}[H]
	\caption{Sort Double}
	\begin{lstlisting}
	void sort_double (int n, double *V_val, int *V_ind) {
		int i, j;
	
		for (i = 1; i <= (n - 1); i++) {
			for (j = i+1; j <= n; j++) {
				if (V_val[j] > V_val[i]) {
					scambia_double(&V_val[j],&V_val[i]);
					scambia(&V_ind[j],&V_ind[i]);
				}
			}
		}
	}
	\end{lstlisting}
	\end{lstfloat}
	Per valutare il costo in termini di tempo di questa procedura bisogna considerare che esegue una passata su tutto l'array, più un'ulteriore passata sulla parte rimanente per ciascun elemento dell'array. Questa seconda fase sarà quindi più lunga nei primi passaggi, mentre si ridurrà notevolmente verso la fine, perchè rimarranno pochi elementi su cui valutare la necessità di uno scambio. Pertanto, il tempo teorico di questa procedura è superiore ad un $\Theta(N)$ (considerando $N$ la dimensione dell'array) ma inferiore ad un $\Theta(N^{2})$.
	

	\subsection{Sperimentazione}
	La sperimentazione è stata effettuata provando diverse combinazioni di parametri, limitatamente al tempo e alla macchina a disposizione e tenendo conto che questa versione di AS è semplificata dal momento che manca la procedura di scambio. I valori usati sono stati:
	\begin{itemize}
		\item $4, 8, 12$ per generazioni e agenti;
		\item $0.1, 0.2$ per il valore di q;
		\item $0.1, 0.2, 0.3$ per il valore di oblivion.
	\end{itemize}
	Riportiamo ora un grafico che mostra i tempi di esecuzione sulle 40 istanze prese in esame e poi un grafico SQD sulle istanze di dimensione 200. Quest'ultimo risulta purtroppo limitato perchè ha senso eseguirlo soltanto su istanze di pari cardinalità, e le istanze disponibili erano raggruppate 4 a 4 per cardinalità.
	\begin{figure}
		\caption{Tempo per Ant System}
		\includegraphics[width=\textwidth]{../img/ant_time.pdf}
	\end{figure}
	\begin{figure}
		\caption{SQD per Ant System}
		\includegraphics[width=\textwidth]{../img/ant_sqd.pdf}
	\end{figure}
\end{document}
